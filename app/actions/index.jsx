var {myFirebase, refFirebase}=require('app/firebase/');
var store = require('./../store/configureStore').storeConfig();

export var setUidOnLogin = function (uid, email) {
    console.log("In Set uid action", uid, email);
    return {
        type: "LOGIN",
        uid: uid,
        email: email,
    };
};

export var logout = function () {
    console.log("In logout function");

    return {
        type: "LOGOUT",
    };
};
export var loginStart = function (email, password) {
    return function (dispatch, getState) {

        myFirebase.auth().signInWithEmailAndPassword(email, password).then(function (result) {

            console.log("Authentication worked", result);
            dispatch(setUidOnLogin(result.uid));

            if (email === 'admin@admin.com' && password === 'admin123') {
                dispatch(roleCarrier('admin'));
            }
            else
                dispatch(roleCarrier('user'));
        }, function (error) {
            console.log("Authentication failed", error);
        });
    }

};
export var signupStart = function (email, password) {
    return function (dispatch, getState) {

        myFirebase.auth().createUserWithEmailAndPassword(email, password).then(function (result) {

            console.log("Authentication worked", result);
            dispatch(setUidOnLogin(result.uid));

            if (email === 'admin@admin.com' && password === 'admin123') {
                dispatch(roleCarrier('admin'));
            }
            else
                dispatch(roleCarrier('user'));

            dispatch(roleCarrier(role));
        }, function (error) {
            console.log("Authentication failed", error);
        });
    }

};
export var roleCarrier = function (role) {
    console.log('at role carrier', role);
    return {
        type: "LOGINCHECK",
        role: role,
    }
};
export var logoutStart = function () {
    return function (dispatch, getState) {
        return myFirebase.auth().signOut().then(function () {
            dispatch(logout());
        });
    }
};
export var loginOrSignUp = function (isSinging) {
    return {
        type: "ISLOGINGORSIGNUP",
        isSinging: isSinging,
    };
};


export var startFetching = function () {
    return {
        type: "STARTING_FETCHING",
    };
};
export var doneFetching = function () {

    return {
        type: "DONE_FETCHING",
    };
};
//////////////////////////////////////Feedback Setting and Fetching/////////////////////////////
export var doneFetchingFeedback = function (feedback) {

    return {
        type: "DONE_FETCHINGFEEDBACK",
        feedback: feedback,
    };
};


export var getFeedback = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());

        var feedback = refFirebase.ref('feedback');
        feedback.on('value', function (snapshot) {
            dispatch(doneFetchingFeedback(snapshot.val()));
        });


    }
};

export var addedFeedback = function (feedback) {
    return {
        type: "ADDED_FEEDBACK",
        feedback: feedback,
    };
}


export var addFeedback = function (feedbackReturned) {
    return function (dispatch, getState) {

        var locationObject = {};
        var state = getState();
        var email = state.loginsignupReducer.email;

        locationObject.text = feedbackReturned;
        locationObject.email = email;

        dispatch(startFetching());
        var feedback = refFirebase.ref('feedback');
        feedback.push(locationObject);
        dispatch(addedFeedback(locationObject));
        dispatch(doneFetching());
    }
};


///////////////////////////////Location Setting and Fetching////////////////////////////////
export var doneFetchingLocation = function (location) {

    return {
        type: "DONE_FETCHINGLOCATION",
        location: location,
    };
};


export var getLocation = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var locations = refFirebase.ref('locations');
        locations.on('value', function (snapshot) {
            dispatch(doneFetchingLocation(snapshot.val()));
        });


    }
};

export var addedLocation = function () {
    return {
        type: "ADDED_LOCATION",

    };
}


export var addLocation = function (locationReturned) {
    return function (dispatch, getState) {

        var locationObject = {};
        locationObject.location = locationReturned;
        dispatch(startFetching());
        var locations = refFirebase.ref('locations');
        locations.push(locationObject);
        dispatch(addedLocation(locationObject));
        dispatch(doneFetching());
    }
};


/////////////////////////Bookings Fetching and Setting//////////////////////////////

export var doneFetchingBooking = function (booking) {

    return {
        type: "DONE_FETCHINGBOOKING",
        booking: booking,
    };
};

export var getBooking = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var state = getState();
        var id = state.loginsignupReducer.uid
        var bookings = refFirebase.ref(`users/${id}/bookings`);
        bookings.on('value', function (snapshot) {
            dispatch(doneFetchingBooking(snapshot.val()));
        });


    }
};

export var addedBooking = function (booking) {
    return {
        type: "ADDED_BOOKING",
        booking: booking,
    };
};

export var addBooking = function (locationKey) {
    return function (dispatch, getState) {
        dispatch(startFetching());

        var location = {};
        var locationObject = refFirebase.ref(`locations/${locationKey}`);
        locationObject.on('value', function (snapshot) {
            location = snapshot.val();
        });
        var state = getState();
        var id = state.loginsignupReducer.uid;
        var bookings = refFirebase.ref(`users/${id}/bookings/${locationKey}`);
        bookings.set(location);
        locationObject.remove();
        dispatch(addedBooking(locationObject));
    }
};
export var cancelBooking = function (bookingObject, key) {
    return function (dispatch, getState) {
        dispatch(startFetching());

        var locationObject = refFirebase.ref(`locations`);
        locationObject.push(bookingObject);
        var state = getState();
        var id = state.loginsignupReducer.uid;

        var bookings = refFirebase.ref(`users/${id}/bookings/${key}`);
        bookings.remove();
        dispatch(addedBooking(bookings));
    }

};

export var cancelBookingAdmin = function (bookingObject, userKey, bookingkey) {
    return function (dispatch, getState) {
        dispatch(startFetching());

        var locationObject = refFirebase.ref(`locations`);
        locationObject.push(bookingObject);

        var bookings = refFirebase.ref(`users/${userKey}/bookings/${bookingkey}`);
        bookings.remove();
        dispatch(addedBooking(bookings));
    }

};

export var getAll = function (users) {

    return {
        type: "GET_ALL_USERS",
        users: users,
    };
};

export var getAllUsers = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var state = getState();
        var users = refFirebase.ref(`users/`);
        users.on('value', function (snapshot) {
            console.log("Usersssssss", snapshot.val());
            dispatch(getAll(snapshot.val()));
        });


    }
};





