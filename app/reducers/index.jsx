export var loginsignupReducer = function (state = {}, action) {

    switch (action.type) {

        case "ISLOGINGORSIGNUP":
            var newState = Object.assign({}, state, {isSigning: action.isSinging});
            return newState;

        case 'LOGIN':
            console.log("In LOGINLOGOUTREDUCER", action.email);
            var newState = Object.assign({}, state, {uid: action.uid, email: action.email});
            return newState;

        case "LOGINCHECK":
            console.log("at reducer", action.role);
            var newState = Object.assign({}, state, {role: action.role});
            return newState;

        case 'LOGOUT':
            return {};
        default:
            return state;
    }
};


////////////////////Feed back Reducer////////////////////////////////////

export var feedBackReducer = function (state = {}, action) {

    switch (action.type) {
        case 'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'DONE_FETCHINGFEEDBACK':
            var newState = Object.assign({}, state, {fetching: false, feedback: action.feedback});
            return newState;

        case 'ADDED_FEEDBACK':
            var newState = Object.assign({}, state, {fetching: false, feedback: action.feedback});
            return newState;
        default:
            return state;
    }
};

export var locationReducer = function (state = {}, action) {

    switch (action.type) {
        case 'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'DONE_FETCHINGLOCATION':
            var newState = Object.assign({}, state, {fetching: false, location: action.location});
            return newState;

        case 'ADDED_LOCATION':
            var newState = Object.assign({}, state, {fetching: false,});
            return newState;
        default:
            return state;
    }
};


export var bookingReducer = function (state = {}, action) {

    switch (action.type) {
        case 'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'DONE_FETCHINGBOOKING':
            var newState = Object.assign({}, state, {fetching: false, booking: action.booking});
            return newState;

        case 'ADDED_BOOKING':
            var newState = Object.assign({}, state, {fetching: false,});
            return newState;

        case 'GET_ALL_USERS':
            var newState = Object.assign({}, state, {fetching: false, users: action.users});
            return newState;
        default:
            return state;


    }
};

