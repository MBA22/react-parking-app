var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');

var actions = require('./../actions/index');
var AdminMain = React.createClass({

    componentDidMount: function () {

        this.props.getLocation();
        this.props.getAllUsers();
    },
    addNewLocation: function (e) {
        e.preventDefault(e);
        var location = this.refs.location.value;
        if (location != null || location != '')
            this.props.addLocation(location);

    },

    deleteBooking: function (bookingObject, userKey, bookingKey) {
        this.props.cancelBookingAdmin(bookingObject, userKey, bookingKey);
    },
    render: function () {
        var _this = this;
        var locations = this.props.locations.location;
        var users = this.props.bookings.users;
        console.log(users);
        console.log(this.props.bookings.fetching);
        if (this.props.locations.location == null || this.props.locations.location == undefined) {
            if (this.props.bookings.users == null || this.props.bookings.users == undefined) {
                return (
                    <div>
                        <h3>Admin Dashboard</h3>
                        <form onSubmit={this.addNewLocation}>
                            <input ref="location" type="text" placeholder="Location"/>
                            <button onClick={this.addNewLocation}>Add New Location</button>
                        </form>

                        <h4>Available Locations:</h4>
                        <p>no location available</p>
                        <br/>
                        <h4>Bookings:</h4>
                        <p>No bookings</p>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <h3>Admin Dashboard</h3>
                        <form onSubmit={this.addNewLocation}>
                            <input ref="location" type="text" placeholder="Location"/>
                            <button onClick={this.addNewLocation}>Add New Location</button>
                        </form>

                        <h4>Available Locations:</h4>
                        <p>no location available</p>
                        <br/>
                        <h4>Bookings:</h4>
                        <ul>
                            {

                                Object.keys(users).map(function (userKeys) {
                                    var bookings = users[userKeys].bookings;
                                    return (
                                        Object.keys(bookings).map(function (bookingKeys) {
                                            return (
                                                <li key={bookingKeys}>{bookings[bookingKeys].location}<span><button
                                                    onClick={this.deleteBooking.bind(this, bookings[bookingKeys], userKeys, bookingKeys)}>Delete</button></span>
                                                </li>
                                            );
                                        }.bind(_this))

                                    );
                                }.bind(_this))
                            }
                        </ul>
                    </div>
                );
            }
        }
        else if (this.props.bookings.users == null || this.props.bookings.users == undefined) {
            return (
                <div>
                    <h3>Admin Dashboard</h3>
                    <form onSubmit={this.addNewLocation}>
                        <input ref="location" type="text" placeholder="Location"/>
                        <button onClick={this.addNewLocation}>Add New Location</button>
                    </form>
                    <h4>Available Locations:</h4>
                    {Object.keys(locations).map(function (key) {
                        return (
                            <div key={key}>
                                <h4>location:{locations[key].location}</h4>
                                <br/>
                            </div>
                        );
                    })}
                    <br/>
                    <h4>Bookings:</h4>
                    <p>No bookings</p>
                </div>
            );
        }
        else return (
                <div>
                    <h3>Admin Dashboard</h3>
                    <form onSubmit={this.addNewLocation}>
                        <input ref="location" type="text" placeholder="Location"/>
                        <button onClick={this.addNewLocation}>Add New Location</button>
                    </form>
                    <h4>Locations:</h4>
                    {Object.keys(locations).map(function (key) {
                        return (
                            <div key={key}>
                                <h4>location:{locations[key].location}</h4>
                                <br/>
                            </div>
                        );
                    })}
                    <br/>
                    <h4>Bookings:</h4>
                    <ul>
                        {
                            Object.keys(users).map(function (userKeys) {
                                var bookings = users[userKeys].bookings;
                                return (
                                    Object.keys(bookings).map(function (bookingKeys) {
                                        return (
                                            <li key={bookingKeys}>{bookings[bookingKeys].location}<span><button
                                                onClick={this.deleteBooking.bind(this, bookings[bookingKeys], userKeys, bookingKeys)}>Delete</button></span>
                                            </li>
                                        );
                                    }.bind(_this))

                                );
                            }.bind(_this))
                        }
                    </ul>

                </div>
            );
    }
});

function mapStateToProps(state) {

    return {
        locations: state.locationReducer,
        bookings: state.bookingReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        addLocation: actions.addLocation,
        getLocation: actions.getLocation,
        getAllUsers: actions.getAllUsers,
        cancelBookingAdmin: actions.cancelBookingAdmin,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(AdminMain);