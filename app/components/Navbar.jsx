var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');
var {Link} = require('react-router');

var Navbar = React.createClass({
    render: function () {
        return (
            <ul>
                <li><Link to="/main">Home</Link></li>
                <li><Link to="/feedback">Feedback</Link></li>
                <li><Link to="/book">Bookings</Link></li>
            </ul>

        );
    },
});

module.exports = Navbar;