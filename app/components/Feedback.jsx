var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');

var actions = require('./../actions/index');
var Feedback = React.createClass({

    componentDidMount: function () {
        this.props.getFeedback();
    },
    addFeedback: function (e) {
        e.preventDefault(e);
        var feedbackText = this.refs.feedback.value;
        if (feedbackText != null || feedbackText != '')
            this.props.addFeedback(feedbackText);

    },
    render: function () {
        var feedBacks = this.props.feedbacks.feedback;
        console.log("feedbacks", feedBacks);
        if (this.props.feedbacks.feedback == null || this.props.feedbacks.feedback == undefined) {
            return (
                <div>
                    <h3>Feedback Component</h3>
                    <form onSubmit={this.addFeedback}>
                        <input ref="feedback" type="text" placeholder="your feedback"/>
                        <button onClick={this.addFeedback}>Add Feedback</button>
                    </form>

                    <h4>FeedBacks:</h4>
                </div>
            );
        }
        else return (
            <div>
                <h3>Feedback Component</h3>
                <form onSubmit={this.addFeedback}>
                    <input ref="feedback" type="text" placeholder="your feedback"/>
                    <button onClick={this.addFeedback}>Add Feedback</button>
                </form>
                <h4>FeedBacks:</h4>
                {Object.keys(feedBacks).map(function (key) {
                    return (
                        <div key={key}>
                            <h3>Feedback:{feedBacks[key].text}</h3>
                            <span>   by: {feedBacks[key].email}</span>
                            <br/>
                        </div>
                    );
                })}
            </div>
        );
    }
});

function mapStateToProps(state) {

    return {
        feedbacks: state.feedBackReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        addFeedback: actions.addFeedback,
        getFeedback: actions.getFeedback,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Feedback);