var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');

var actions = require('./../actions/index');
var selectedLocation = '';
var Book = React.createClass({

    componentDidMount: function () {
        this.props.getLocation();
        this.props.getBooking();
    },
    bookParking: function (e) {
        e.preventDefault(e);
        var locationKey = this.refs.location.value;
        console.log("Location Key for booking", locationKey)
        if (locationKey != null || locationKey != '')
            this.props.addBooking(locationKey);

    },
    deleteBooking: function (locationObject, key) {
        this.props.cancelBooking(locationObject, key);
    },

    render: function () {
        var locations = this.props.locations.location;
        var bookings = this.props.bookings.booking;
        if (this.props.bookings.fetching) {
            return (<p>loading...</p>);
        }
        if (locations == null || locations == undefined) {
            if (bookings == null || bookings == undefined)
                return (
                    <div>
                        <p>No Parking location available</p>
                        <p>No Bookings ye</p>
                    </div>
                );
            else return (
                <div>
                    <p>no location available for parking</p>
                    <h3>Bookings:</h3>
                    <ul>
                        {Object.keys(bookings).map(function (key) {
                            return (
                                <li key={key}>{bookings[key].location}<span><button
                                    onClick={this.deleteBooking.bind(this, bookings[key], key)}>Cancel Booking</button></span>
                                </li>
                            );
                        }.bind(this))}
                    </ul>
                </div>
            );
        }
        else if (bookings == null || bookings == undefined) {
            return (
                <div>
                    <h3>Book Parking</h3>
                    <form onSubmit={this.bookParking}>
                        <select ref="location" id="locations">
                            <option value=''>Select location</option>
                            {Object.keys(locations).map(function (key) {
                                return (
                                    <option key={key} value={key}>{locations[key].location}</option>
                                );
                            })}
                        </select>
                        <button onClick={this.bookParking}>Book Parking</button>
                    </form>

                    <h3>Bookings:</h3>
                    <p>No bookingd Yet</p>
                </div>

            );
        }
        else
            return (
                <div>
                    <h3>Book Parking</h3>
                    <form onSubmit={this.bookParking}>
                        <select ref="location" id="locations">
                            <option value=''>Select location</option>
                            {Object.keys(locations).map(function (key) {
                                return (
                                    <option key={key} value={key}>{locations[key].location}</option>
                                );
                            })}
                        </select>
                        <button onClick={this.bookParking}>Book Parking</button>
                    </form>

                    <h3>Bookings:</h3>
                    <ul>
                        {Object.keys(bookings).map(function (key) {
                            return (
                                <li key={key}>{bookings[key].location}<span><button
                                    onClick={this.deleteBooking.bind(this, bookings[key], key)}>Cancel Booking</button></span>
                                </li>
                            );
                        }.bind(this))}
                    </ul>
                </div>

            );
    }
});

function mapStateToProps(state) {

    return {
        locations: state.locationReducer,
        bookings: state.bookingReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
            getLocation: actions.getLocation,
            addBooking: actions.addBooking,
            getBooking: actions.getBooking,
            cancelBooking: actions.cancelBooking,
        }, dispatch
    );
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Book);